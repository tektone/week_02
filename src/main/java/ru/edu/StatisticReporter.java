package ru.edu;

/**
 * Интерфейс для генерации отчета с полученной статистикой.
 */
public interface StatisticReporter {

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    void report(TextStatistics statistics);
}
